#include <cstdio>
#include <dirent.h>
#include <sys/types.h>

// ref.
// https://stackoverflow.com/a/12506
// https://stackoverflow.com/a/612176
//

int main(int argc, char *argv[])
{
    const char *dirname;
    if (argc <= 1) {
        dirname = ".";
    }
    else {
        dirname = argv[1];       
    }
            
    DIR *dir;
    dirent *directory_entry;
    if ( (dir = opendir(dirname)) != NULL ) {
        while ( (directory_entry = readdir(dir)) != NULL ) {
            printf("%s\t", directory_entry->d_name);
        }
        puts("");
        closedir(dir);
    }
    else {
        puts("No such file or directory");
    }
}
