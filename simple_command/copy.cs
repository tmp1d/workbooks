using System.IO;
//using System.Text;

// ref.
// https://docs.microsoft.com/ko-kr/dotnet/csharp/programming-guide/file-system/how-to-copy-delete-and-move-files-and-folders
// https://msdn.microsoft.com/ko-kr/library/system.io.filestream.read(v=vs.110).aspx
// https://msdn.microsoft.com/ko-kr/library/system.io.filestream.write(v=vs.110).aspx
// https://stackoverflow.com/a/381529
//

var args = Environment.GetCommandLineArgs();
bool move_flag = false;
bool raw_flag  = false;

class Copy
{
    static string source_s, target_s;

    public static void Program()
    {
        CheckOptions();
        if (source_s == null) {
            Console.WriteLine("usage: csharp copy.cs -- [-M, --move | -R, --raw] source_file target_file");
            Environment.Exit(0);
        }
        else if (target_s == null) {
            Console.WriteLine("target filename is required");
            Environment.Exit(0);
        }

        try {
            if (raw_flag) {
                var fs = new FileStream(source_s, FileMode.Open, FileAccess.Read);
                var fsw = new FileStream(target_s, FileMode.Create, FileAccess.Write);
                var buffer = new byte[fs.Length];
                
                int size;
                while ( (size = fs.Read(buffer, 0, buffer.Length)) > 0 ) {
                    //var utf8 = new UTF8Encoding(true);
                    //Console.WriteLine(utf8.GetString(buffer));
                    fsw.Write(buffer, 0, size);
                }
                //System.IO.File.WriteAllBytes(target_s, buffer);
            }
            else {
                File.Copy(source_s, target_s, true);
            }
        
            if (move_flag) RemoveFile();
        } catch (System.Exception) {
            Console.WriteLine("No such file or directory");
        }
    }
    
    static void CheckOptions()
    {
        for (var i=3; i < args.Length; i++) {
            bool skip = false;
            switch (args[i]) {
                case "-M": case "--move":
                    move_flag = skip = true; break;
                case "-R": case "--raw":
                    raw_flag = skip = true; break;
            } if (skip) continue;
            
            if (source_s == null) {
                source_s = args[i];
            }
            else {
                target_s = args[i];
            }
        }
    }
    
    static void RemoveFile()
    {
        File.Delete(source_s);
    }
}



Copy.Program();


